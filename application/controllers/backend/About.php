<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if($this->session->userdata('role') != 'admin'){
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index(){
		$data['about']=$this->db->query("Select * from about")->row();
		$this->render_backend('backend/about',$data);
	}
	public function update($id){
		$tittle=$_POST['tittle'];
		$isi=$_POST['isi'];
		$file_lama=$_POST['file_lama'];
		if(isset($_POST)){
			$config['upload_path']          = './assets/frontend/img/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			if($_FILES['gambar']['name']!=""){
				unlink("assets/frontend/img/".$file_lama);
				$this->upload->do_upload('gambar');
				$data=$this->upload->data();
				$file=$data['file_name'];
				$data=array('tittle' => $tittle,
					'isi' => $isi,
					'gambar' => $file,
				);
				$this->db->update('about', $data, array('id' => $id));
			}
			else{
				$data=array('tittle' => $tittle,
					'isi' => $isi,
					'gambar' => $file_lama,
				);
				$this->db->update('about', $data, array('id' => $id));
			}
			if($this->db->affected_rows()>0){
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik berhasil diedit !</div></div>');
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik gagal diedit !</div></div>');
			}
		}
		redirect('backend/about');
	}
}
