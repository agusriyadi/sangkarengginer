<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if($this->session->userdata('role') != 'admin'){
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index(){
		$data['team']=$this->db->query("Select * from team")->result();
		$this->render_backend('backend/team',$data);
	}
	public function tambah(){
		$this->render_backend('backend/team_add');
	}
	public function insert(){
		// var_dump($_POST);die;
		if(isset($_POST)){
			$nama=$_POST['nama'];
			$jabatan=$_POST['jabatan'];
			$deskripsi=$_POST['deskripsi'];
			$config['upload_path']          = './assets/frontend/img/team/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$data=$this->upload->data();
			$file=$data['file_name'];
			$data=array('nama'=>$nama,
				'jabatan'=>$jabatan,
				'deskripsi'=>$deskripsi,
				'gambar'=>$file
			);
			$this->db->insert('team',$data);
			if($this->db->affected_rows()>0){
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team berhasil ditambah !</div></div>');
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team gagal ditambah</div></div>');
			}
			redirect('backend/team');
		}
	}
	public function edit($id){
		$data['team']=$this->db->query("Select * from team where id_team='$id'")->row();
		$this->render_backend('backend/team_edit',$data);
	}
	public function update($id){
		// var_dump($_POST);die;
		if(isset($_POST)){
			$nama=$_POST['nama'];
			$jabatan=$_POST['jabatan'];
			$deskripsi=$_POST['deskripsi'];
			$file_lama=$_POST['file_lama'];
			$config['upload_path']          = './assets/frontend/img/team/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			if($_FILES['gambar']['name']!=""){
				unlink("assets/frontend/img/team/".$file_lama);
				$this->upload->do_upload('gambar');
				$data=$this->upload->data();
				$file=$data['file_name'];
				$data=array('nama' => $nama,
					'jabatan' => $jabatan,
					'deskripsi' => $deskripsi,
					'gambar'=>$file
				);
				$this->db->update('team', $data, array('id_team' => $id));
			}
			else{
				$data=array('nama' => $nama,
					'jabatan' => $jabatan,
					'deskripsi' => $deskripsi,
					'gambar'=>$file_lama
				);
				$this->db->update('team', $data, array('id_team' => $id));
			}
			if($this->db->affected_rows()>0){
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team berhasil diedit !</div></div>');
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team gagal diedit !</div></div>');
			}
		}
		redirect('backend/team');
	}
	public function delete($id){
		$this->db->delete('team', array('id_team' => $id));
		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team berhasil dihapus !</div></div>');
		}
		else{
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Team gagal dihapus !</div></div>');
		}
		redirect('backend/portfolio');
	}
}
