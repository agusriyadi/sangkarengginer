<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if($this->session->userdata('role') != 'admin'){
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index(){
		$data['contact']=$this->db->query("Select * from contact")->row();
		$this->render_backend('backend/contact',$data);
	}
	public function update($id){
		if(isset($_POST)){
			$lokasi=$_POST['lokasi'];
			$email=$_POST['email'];
			$phone=$_POST['phone'];
			$data=array('lokasi' => $lokasi,
				'email' => $email,
				'phone' => $phone,
			);
			$this->db->update('contact', $data, array('id' => $id));
			if($this->db->affected_rows()>0){
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik berhasil diedit !</div></div>');
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik gagal diedit !</div></div>');
			}
		}
		redirect('backend/contact');
	}
}
