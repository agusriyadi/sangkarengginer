<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin') {
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index()
	{
		// function render_backend tersebut dari file core/MY_Controller.php
		$this->render_backend('backend/dashboard'); // load view home.php
	}
}
