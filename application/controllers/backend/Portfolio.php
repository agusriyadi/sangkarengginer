<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Portfolio extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if ($this->session->userdata('role') != 'admin') {
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index()
	{
		$data['portfolio'] = $this->db->query("Select * from portfolio")->result();
		$this->render_backend('backend/portfolio', $data);
	}
	public function tambah()
	{
		$this->render_backend('backend/portfolio_add');
	}
	public function insert()
	{
		// var_dump($_POST);die;
		if (isset($_POST)) {
			$nama_port = $_POST['nama_port'];
			$detail = $_POST['detail'];
			// $kategori=$_POST['kategori'];
			$kategori = 'None';
			$config['upload_path']          = './assets/frontend/img/portfolio/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$data = $this->upload->data();
			$file = $data['file_name'];
			$data = array(
				'nama_port' => $nama_port,
				'detail' => $detail,
				'kategori' => $kategori,
				'gambar' => $file
			);
			$this->db->insert('portfolio', $data);
			if ($this->db->affected_rows() > 0) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan berhasil ditambah !</div></div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan gagal ditambah</div></div>');
			}
			redirect('backend/portfolio');
		}
	}
	public function edit($id)
	{
		$data['portfolio'] = $this->db->query("Select * from portfolio where id_portfolio='$id'")->row();
		$this->render_backend('backend/portfolio_edit', $data);
	}
	public function update($id)
	{
		// var_dump($_POST);die;
		if (isset($_POST)) {
			$nama_port = $_POST['nama_port'];
			$detail = $_POST['detail'];
			// $kategori = $_POST['kategori'];
			$kategori = 'None';
			$file_lama = $_POST['file_lama'];
			$config['upload_path']          = './assets/frontend/img/portfolio/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			if ($_FILES['gambar']['name'] != "") {
				unlink("assets/frontend/img/portfolio/" . $file_lama);
				$this->upload->do_upload('gambar');
				$data = $this->upload->data();
				$file = $data['file_name'];
				$data = array(
					'nama_port' => $nama_port,
					'detail' => $detail,
					'kategori' => $kategori,
					'gambar' => $file
				);
				$this->db->update('portfolio', $data, array('id_portfolio' => $id));
			} else {
				$data = array(
					'nama_port' => $nama_port,
					'detail' => $detail,
					'kategori' => $kategori,
					'gambar' => $file_lama
				);
				$this->db->update('portfolio', $data, array('id_portfolio' => $id));
			}
			if ($this->db->affected_rows() > 0) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Portfolio berhasil diedit !</div></div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Portfolio gagal diedit !</div></div>');
			}
		}
		redirect('backend/portfolio');
	}
	public function delete($id)
	{
		$this->db->delete('portfolio', array('id_portfolio' => $id));
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Portfolio berhasil dihapus !</div></div>');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Portfolio gagal dihapus !</div></div>');
		}
		redirect('backend/portfolio');
	}
}
