<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Services extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if ($this->session->userdata('role') != 'admin') {
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index()
	{
		$data['service'] = $this->db->query("Select * from service")->result();
		$this->render_backend('backend/service', $data);
	}
	public function tambah()
	{
		$this->render_backend('backend/service_add');
	}
	public function insert()
	{
		if (isset($_POST)) {
			$pelayanan = $_POST['pelayanan'];
			$deskripsi = $_POST['deskripsi'];
			$icon = $_POST['icon'];
			$data = array(
				'pelayanan' => $pelayanan,
				'deskripsi' => $deskripsi,
				'icon' => $icon
			);
			$this->db->insert('service', $data);
			if ($this->db->affected_rows() > 0) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan berhasil ditambah !</div></div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan gagal ditambah</div></div>');
			}
		}
		redirect('backend/services');
	}
	public function edit($id)
	{
		$data['service'] = $this->db->query("Select * from service where id_service='$id'")->row();
		$this->render_backend('backend/service_edit', $data);
	}
	public function update($id)
	{
		if (isset($_POST)) {
			$pelayanan = $_POST['pelayanan'];
			$deskripsi = $_POST['deskripsi'];
			$icon = $_POST['icon'];
			$data = array(
				'pelayanan' => $pelayanan,
				'deskripsi' => $deskripsi,
				'icon' => $icon
			);
			$this->db->update('service', $data, array('id_service' => $id));
			if ($this->db->affected_rows() > 0) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan berhasil diedit !</div></div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan gagal diedit !</div></div>');
			}
		}
		redirect('backend/services');
	}
	public function delete($id)
	{
		$this->db->delete('service', array('id_service' => $id));
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan berhasil dihapus !</div></div>');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Pelayanan gagal dihapus !</div></div>');
		}
		redirect('backend/services');
	}
}
