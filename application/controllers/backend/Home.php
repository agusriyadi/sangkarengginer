<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
		if($this->session->userdata('role') != 'admin'){
			show_404(); // Redirect ke halaman 404 Not found
		}
	}
	public function index(){
		$data['home']=$this->db->query("Select * from home")->row();
		$this->render_backend('backend/home',$data);
	}
	public function update($id){
		$head1=$_POST['head1'];
		$head2=$_POST['head2'];
		$file_lama=$_POST['file_lama'];
		if(isset($_POST)){
			$config['upload_path']          = './assets/frontend/img/';
			$config['allowed_types']        = 'jpg|png';
			// $config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);
			if($_FILES['banner']['name']!=""){
				unlink("assets/frontend/img/".$file_lama);
				$this->upload->do_upload('banner');
				$data=$this->upload->data();
				$file=$data['file_name'];
				$data=array('head1' => $head1,
					'head2' => $head2,
					'banner' => $file,
				);
				$this->db->update('home', $data, array('id' => $id));
			}
			else{
				$data=array('head1' => $head1,
					'head2' => $head2,
					'banner' => $file_lama,
				);
				$this->db->update('home', $data, array('id' => $id));
			}
			if($this->db->affected_rows()>0){
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik berhasil diedit !</div></div>');
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>&times;</span></button>Data Kaldik gagal diedit !</div></div>');
			}
		}
		redirect('backend/home');
	}
}
