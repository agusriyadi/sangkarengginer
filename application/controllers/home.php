<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{
		$data = [
			'data' => $this->db->query("Select * from home,about,contact")->row(),
			'service' => $this->db->query("Select * from service")->result(),
			'portfolio' => $this->db->query("Select * from portfolio")->result(),
			'team' => $this->db->query("Select * from team")->result()
		];
		$this->template->load('template', 'Home', $data);
	}
}
