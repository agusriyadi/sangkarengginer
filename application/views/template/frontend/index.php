<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sangkar Engineering</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url('assets/logo.jpg'); ?>" rel="icon">
  <link href="<?php echo base_url('assets/logo.jpg'); ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url('assets/frontend/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url('assets/frontend/css/style.css'); ?>" rel="stylesheet">

  <!-- =======================================================
  * Template Name: eNno - v2.2.1
  * Template URL: https://bootstrapmade.com/enno-free-simple-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <?php
        /*
        * Variabel $headernya diambil dari core MY_Controller
        * (application/core/MY_Controller.php)
        * */
        echo $headernya;
        ?>
        
        <?php
        /*
        * Variabel $contentnya diambil dari core MY_Controller
        * (application/core/MY_Controller.php)
        * */
        echo $contentnya;
        ?>
        <!-- ======= Footer ======= -->
        <footer id="footer">

            <div class="footer-top">

              <div class="container">

                <div class="row  justify-content-center">
                  <div class="col-lg-6">
                    <h3>eNno</h3>
                    <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
                </div>
            </div>

            <div class="row footer-newsletter justify-content-center">
              <div class="col-lg-6">
                <form action="" method="post">
                  <input type="email" name="email" placeholder="Enter your Email"><input type="submit" value="Subscribe">
              </form>
          </div>
      </div>

      <div class="social-links">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>

  </div>
</div>

<div class="container footer-bottom clearfix">
  <div class="copyright">
    &copy; Copyright <strong><span>eNno</span></strong>. All Rights Reserved
</div>
<div class="credits">
    <!-- All the links in the footer should remain intact. -->
    <!-- You can delete the links only if you purchased the pro version. -->
    <!-- Licensing information: https://bootstrapmade.com/license/ -->
    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
</div>
</div>
</footer><!-- End Footer -->
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="<?php echo base_url('assets/frontend/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/jquery.easing/jquery.easing.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/php-email-form/validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/waypoints/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/counterup/counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/venobox/venobox.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>

<!-- Template Main JS File -->
<script src="<?php echo base_url('assets/frontend/js/main.js'); ?>"></script>

</body>

</html>