<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <!-- frontend -->
    <!-- Favicons -->
    <link href="<?php echo base_url('assets/logo.jpg'); ?>" rel="icon">
    <link href="<?php echo base_url('assets/logo.jpg'); ?>" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?php echo base_url('assets/frontend/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?php echo base_url('assets/frontend/css/style.css'); ?>" rel="stylesheet">
    <!-- end frontend -->
    <!-- Title Page-->
    <title>Dashboard Admin | SANGKAR ENGINEREEING</title>
    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url('assets/backend/css/font-face.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/font-awesome-4.7/css/font-awesome.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/font-awesome-5/css/fontawesome-all.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/mdi-font/css/material-design-iconic-font.min.css'); ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url('assets/backend/vendor/bootstrap-4.1/bootstrap.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/bootstrap-4.1/bootstrap.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/bootstrap-4.1/bootstrap4.min.css'); ?>" rel="stylesheet" media="all">
    <!-- Vendor CSS-->
    <link href="<?php echo base_url('assets/backend/vendor/animsition/animsition.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/wow/animate.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/css-hamburgers/hamburgers.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/slick/slick.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/select2/select2.min.css'); ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/backend/vendor/perfect-scrollbar/perfect-scrollbar.css'); ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url('assets/backend/css/theme.css'); ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div style="background-color: #0c407a;" class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <!-- <img style="height:52px" src="<?php echo base_url('assets/backend/images/icon/logo-blue.png'); ?>" /> -->
                            <img style="height:52px" src="<?php echo base_url('assets/logoweight.jpg'); ?>" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <?php
                        $aktif = $this->uri->segment(1);
                        $aktifmenu = $this->uri->segment(2);
                        ?>
                        <li class="has-sub <?= ($aktifmenu == 'dashboard') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/dashboard') ?>">
                                <i class="fas fa-tachometer-alt"></i>Dashboard
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'home') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/home') ?>">
                                <i class="fas fa-user"></i>Home
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'about') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/about') ?>">
                                <i class="fas fa-asterisk"></i>About
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'services') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/services') ?>">
                                <i class="fas fa-tasks"></i>Services
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'portfolio') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/portfolio') ?>">
                                <i class="fas fa-tasks"></i>Portfolio
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'team') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/team') ?>">
                                <i class="fas fa-tasks"></i>Team
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'contact') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/contact') ?>">
                                <i class="fas fa-tasks"></i>Contact
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'logout') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?php echo base_url('auth/logout'); ?>">
                                <i class="fas fa-tasks"></i>Logout
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div style="background-color: #0c407a;" class="logo">
                <a href="#">
                    <img style="height:52px" src="<?php echo base_url('assets/logoweight.jpg'); ?>" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php
                        $aktif = $this->uri->segment(1);
                        $aktifmenu = $this->uri->segment(2);
                        ?>
                        <li class="has-sub <?= ($aktifmenu == 'dashboard') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/dashboard') ?>">
                                <i class="fas fa-tachometer-alt"></i>Dashboard
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'home') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/home') ?>">
                                <i class="fas fa-user"></i>Home
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'about') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/about') ?>">
                                <i class="fas fa-asterisk"></i>About
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'services') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/services') ?>">
                                <i class="fas fa-tasks"></i>Services
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'portfolio') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/portfolio') ?>">
                                <i class="fas fa-tasks"></i>Portfolio
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'team') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/team') ?>">
                                <i class="fas fa-tasks"></i>Team
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'contact') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?= base_url('backend/contact') ?>">
                                <i class="fas fa-tasks"></i>Contact
                            </a>
                        </li>
                        <li class="has-sub <?= ($aktifmenu == 'logout') ? 'active' : ''; ?>">
                            <a class="js-arrow" href="<?php echo base_url('auth/logout'); ?>">
                                <i class="fas fa-tasks"></i>Logout
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="form-header">
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $this->session->userdata('username') ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account
                                                    </a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="<?php echo base_url('auth/logout'); ?>">
                                                    <i class="zmdi zmdi-power"></i>Logout
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <?php echo $contentnya; ?>

            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <!-- frontend -->
    <!-- Vendor JS Files -->
    <script src="<?php echo base_url('assets/frontend/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/jquery.easing/jquery.easing.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/php-email-form/validate.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/waypoints/jquery.waypoints.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/counterup/counterup.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/venobox/venobox.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/frontend/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>

    <!-- Template Main JS File -->
    <script src="<?php echo base_url('assets/frontend/js/main.js'); ?>"></script>
    <!-- endfrontend -->
    <!-- Jquery JS-->
    <script src="<?php echo base_url('assets/backend/vendor/jquery-3.2.1.min.js'); ?>"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url('assets/backend/vendor/bootstrap-4.1/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/bootstrap-4.1/bootstrap.min.js'); ?>"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url('assets/backend/vendor/slick/slick.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/backend/vendor/wow/wow.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/animsition/animsition.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/backend/vendor/counter-up/jquery.waypoints.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/counter-up/jquery.counterup.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/backend/vendor/circle-progress/circle-progress.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/perfect-scrollbar/perfect-scrollbar.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/chartjs/Chart.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/select2/select2.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/backend/vendor/vector-map/jquery.vmap.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/vector-map/jquery.vmap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/vector-map/jquery.vmap.sampledata.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/vector-map/jquery.vmap.world.js'); ?>"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url('assets/backend/js/main.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/js/jquery-3.3.1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/backend/js/dataTables.bootstrap4.min.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
</body>

</html>
<!-- end document-->