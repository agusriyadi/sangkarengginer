<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Edit</strong>
                            <small> Portfolio</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url() . 'backend/portfolio/update/' . $portfolio->id_portfolio ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Nama Portfolio</label>
                                    <input type="text" name="nama_port" placeholder="" class="form-control" value="<?= $portfolio->nama_port ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Detail</label>
                                    <input type="text" name="detail" placeholder="" class="form-control" value="<?= $portfolio->detail ?>">
                                </div>
                                <!-- <div class="form-group">
                                    <label for="vat" class=" form-control-label">Kategori</label>
                                    <select name="kategori" class="form-control">
                                        <option <?php if ($portfolio->kategori == "App") {
                                                    echo "selected";
                                                } ?> value="App">App</option>
                                        <option <?php if ($portfolio->kategori == "Web") {
                                                    echo "selected";
                                                } ?> value="Web">Web</option>
                                        <option <?php if ($portfolio->kategori == "Card") {
                                                    echo "selected";
                                                } ?> value="Card">Card</option>
                                    </select>
                                </div> -->
                                <div class="form-group">
                                    <a href="<?php echo base_url('assets/frontend/img/portfolio/' . $portfolio->gambar); ?>">
                                        <img width="200px" src="<?php echo base_url('assets/frontend/img/portfolio/' . $portfolio->gambar); ?>">
                                    </a>
                                </div>
                                <input type="hidden" name="file_lama" value="<?= $portfolio->gambar ?>">
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Ubah Gambar</label>
                                    <input type="file" name="gambar" placeholder="" class="form-control">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>