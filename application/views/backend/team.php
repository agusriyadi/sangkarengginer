<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">Kelola Team</h3>
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                           <a href="<?= base_url('backend/team/tambah') ?>">
                            <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                             <i class="zmdi zmdi-plus"></i>Tambah Team
                         </button></a>
                     </div>
                 </div>
                 <div class="table-responsive table-responsive-data2">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Deskripsi</th>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($team as $team){ ?> 
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $team->nama ?></td>
                                    <td><?= $team->jabatan ?></td>
                                    <td><?= $team->deskripsi ?></td>
                                    <td><a target="_blank" href="<?php echo base_url('assets/frontend/img/team/'.$team->gambar); ?>"><img width="200px" src="<?php echo base_url('assets/frontend/img/team/'.$team->gambar); ?>"></a></td>
                                    <td>
                                        <div class="table-data-feature">
                                            <a href="<?= base_url('backend/team/edit/'.$team->id_team) ?>">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </a>
                                            <a href="<?=base_url('backend/team/delete'.$team->id_team)?>">
                                                <button onclick="return confirm('Apakah anda yakin ?')" type="submit" class="item"><i class="zmdi zmdi-delete"></i></i></button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++;} ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
        </div>
    </div>
</div>
