<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Edit</strong>
                            <small> Team</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'backend/team/update/'.$team->id_team ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Nama </label>
                                    <input type="text" name="nama" placeholder="" class="form-control" value="<?= $team->nama ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Jabatan</label>
                                    <input type="text" name="jabatan" placeholder="" class="form-control" value="<?= $team->jabatan ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Deskripsi</label>
                                    <input type="text" name="deskripsi" placeholder="" class="form-control" value="<?= $team->deskripsi ?>">
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url('assets/frontend/img/team/'.$team->gambar); ?>">
                                      <img width="200px" src="<?php echo base_url('assets/frontend/img/team/'.$team->gambar); ?>">
                                  </a>
                              </div>
                              <input type="hidden" name="file_lama" value="<?= $team->gambar ?>">
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Gambar</label>
                                    <input type="file" name="gambar" placeholder="" class="form-control" value="<?= $team->gambar ?>">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>