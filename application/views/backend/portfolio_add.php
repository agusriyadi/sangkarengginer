<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tambah</strong>
                            <small> Portfolio</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url() . 'backend/portfolio/insert' ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Nama Portfolio</label>
                                    <input type="text" name="nama_port" placeholder="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Detail</label>
                                    <input type="text" name="detail" placeholder="" class="form-control">
                                </div>
                                <!-- <div class="form-group">
                                    <label for="vat" class=" form-control-label">Kategori</label>
                                    <select name="kategori" class="form-control">
                                        <option disabled selected>--Pilih Kategori--</option>
                                        <option value="App">App</option>
                                        <option value="Web">Web</option>
                                        <option value="Card">Card</option>
                                    </select>
                                </div> -->
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Gambar</label>
                                    <input type="file" name="gambar" placeholder="" class="form-control">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>