<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tambah</strong>
                            <small> Service</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'backend/services/insert' ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Pelayanan</label>
                                    <input type="text" name="pelayanan" placeholder="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Deskripsi</label>
                                    <input type="text" name="deskripsi" placeholder="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Icon</label>
                                    <input type="text" name="icon" placeholder="" class="form-control">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>