<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Setting</strong>
                            <small> Contact</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'backend/contact/update/'.$contact->id; ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Lokasi</label>
                                    <input type="text" name="lokasi" placeholder="" class="form-control" value="<?= $contact->lokasi ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Email</label>
                                    <input type="text" name="email" placeholder="" class="form-control" value="<?= $contact->email ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Phone</label>
                                    <input type="text" name="phone" placeholder="" class="form-control" value="<?= $contact->phone ?>">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>