<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">Kelola Pelayanan</h3>
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="table-data__tool">
                        <div class="table-data__tool-right">
                         <a href="<?= base_url('backend/services/tambah') ?>">
                            <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                               <i class="zmdi zmdi-plus"></i>Tambah Pelayanan
                           </button></a>
                       </div>
                   </div>
                   <div class="table-responsive table-responsive-data2">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pelayanan</th>
                                <th>Deskripsi</th>
                                <th>Icon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($service as $serv){ ?> 
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?php echo $serv->pelayanan ?></td>
                                    <td><?= $serv->deskripsi ?></td>
                                    <td> <div class="icon"><i class="bx <?= $serv->icon ?>"></i></div></td>
                                    <td>
                                        <div class="table-data-feature">
                                            <a href="<?= base_url('backend/services/edit/'.$serv->id_service) ?>">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </a>
                                            <a href="<?=base_url('backend/services/delete/'.$serv->id_service)?>">
                                                <button onclick="return confirm('Apakah anda yakin ?')" type="submit" class="item"><i class="zmdi zmdi-delete"></i></i></button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++;} ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE -->
                </div>
            </div>
        </div>
    </div>
</div>
