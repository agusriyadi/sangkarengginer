<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Setting</strong>
                            <small> About</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url() . 'backend/about/update/' . $about->id; ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Judul</label>
                                    <input type="text" name="tittle" placeholder="" class="form-control" value="<?= $about->tittle ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Isi</label>
                                    <input type="text" name="isi" placeholder="" class="form-control" value="<?= $about->isi ?>">
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url('assets/frontend/img/' . $about->gambar); ?>">
                                        <img width="500px" src="<?php echo base_url('assets/frontend/img/' . $about->gambar); ?>">
                                    </a>
                                </div>
                                <input type="hidden" name="file_lama" value="<?= $about->gambar ?>">
                                <div class="form-group">
                                    <label>Ubah Gambar</label>
                                    <input accept=".jpg, .png" type="file" class="form-control" name="gambar">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script>
    CKEDITOR.replace('exampleFormControlTextarea1');
</script> -->