<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Setting</strong>
                            <small> Home</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url().'backend/home/update/'.$home->id; ?>">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Header 1</label>
                                    <input type="text" name="head1" placeholder="" class="form-control" value="<?= $home->head1 ?>">
                                </div>
                                <div class="form-group">
                                    <label for="vat" class=" form-control-label">Header 2</label>
                                    <input type="text" name="head2" placeholder="" class="form-control" value="<?= $home->head2 ?>">
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url('assets/frontend/img/'.$home->banner); ?>">
                                      <img width="500px" src="<?php echo base_url('assets/frontend/img/'.$home->banner); ?>">
                                  </a>
                              </div>
                              <input type="hidden" name="file_lama" value="<?= $home->banner ?>">
                              <div class="form-group">
                                <label >Ubah Banner</label>
                                <input accept=".jpg, .png" type="file" class="form-control" name="banner">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>