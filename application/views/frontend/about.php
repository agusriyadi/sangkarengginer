<!-- ======= About Section ======= -->
<section id="about" class="about">
    <div class="container">
        <div class="section-title">
            <!-- <span>Services</span> -->
            <h2>About Company</h2>
            <!-- <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p> -->
        </div>

        <div class="row">
            <div class="col-lg-6">
                <img src="<?php echo base_url('assets/frontend/img/' . $data->gambar); ?>" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 content">
                <h3><?= $data->tittle ?></h3>
                <?= $data->isi ?>
            </div>
        </div>

    </div>
</section><!-- End About Section -->

<!-- ======= Counts Section ======= -->
<section id="counts" class="counts">
    <div class="container">

        <!-- <div class="row counters">

      <div class="col-lg-3 col-6 text-center">
        <span data-toggle="counter-up">232</span>
        <p>Clients</p>
      </div>

      <div class="col-lg-3 col-6 text-center">
        <span data-toggle="counter-up">521</span>
        <p>Projects</p>
      </div>

      <div class="col-lg-3 col-6 text-center">
        <span data-toggle="counter-up">1,463</span>
        <p>Hours Of Support</p>
      </div>

      <div class="col-lg-3 col-6 text-center">
        <span data-toggle="counter-up">15</span>
        <p>Hard Workers</p>
      </div>

    </div> -->

    </div>
</section><!-- End Counts Section -->