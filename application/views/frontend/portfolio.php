<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
  <div class="container">

    <div class="section-title">
      <span>Portfolio</span>
      <h2>Portfolio</h2>
      <p>SOLVE YOUR DAILY TO YOUR MOST ADVANCED ENGINEERING PROBLEMS !!!</p>
    </div>

    <!-- <div class="row">
        <div class="col-lg-12 d-flex justify-content-center">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">All</li>
            <li data-filter=".filter-app">App</li>
            <li data-filter=".filter-card">Card</li>
            <li data-filter=".filter-web">Web</li>
          </ul>
        </div>
      </div> -->

    <div class="row portfolio-container">
      <?php foreach ($portfolio as $port) { ?>
        <div class="col-lg-4 col-md-6 portfolio-item <?php if ($port->kategori == "App") {
                                                        echo "filter-app";
                                                      } elseif ($port->kategori == "Web") {
                                                        echo "filter-web";
                                                      } else {
                                                        echo "filter-card";
                                                      } ?>">
          <img src="<?php echo base_url('assets/frontend/img/portfolio/' . $port->gambar); ?>" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4><?= $port->nama_port ?></h4>
            <!-- <p><?= $port->kategori ?></p> -->
            <a href="<?php echo base_url('assets/frontend/img/portfolio/' . $port->gambar); ?>" data-gall="portfolioGallery" class="venobox preview-link"><i class="bx bx-plus"></i></a>
            <!-- <a href="portfolio-details.html" class="details-link"><i class="bx bx-link"></i></a> -->
          </div>
        </div>
      <?php } ?>
    </div>

  </div>
</section><!-- End Portfolio Section -->