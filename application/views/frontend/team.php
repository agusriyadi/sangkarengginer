<!-- ======= Team Section ======= -->
<section id="team" class="team section-bg">
  <div class="container">

    <div class="section-title">
      <span>Team</span>
      <h2>Team</h2>
      <p>Respect, Integrity, Growth, Honesty, Team Works</p>
    </div>

    <div class="row">
      <?php foreach ($team as $t) { ?>
        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
          <div class="member">
            <img src="<?php echo base_url('assets/frontend/img/team/' . $t->gambar); ?>" alt="">
            <h4><?= $t->nama ?></h4>
            <span><?= $t->jabatan ?></span>
            <p>
              <?= $t->deskripsi ?>
            </p>
            <!-- <div class="social">
              <a href=""><i class="icofont-twitter"></i></a>
              <a href=""><i class="icofont-facebook"></i></a>
              <a href=""><i class="icofont-instagram"></i></a>
              <a href=""><i class="icofont-linkedin"></i></a>
            </div> -->
          </div>
        </div>
      <?php } ?>

    </div>

  </div>
</section><!-- End Team Section -->