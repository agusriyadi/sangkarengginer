<!-- ======= Services Section ======= -->
<section id="services" class="services section-bg">
  <div class="container">

    <div class="section-title">
      <span>Services</span>
      <h2>Services</h2>
      <!-- <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p> -->
    </div>

    <div class="row">
      <?php foreach ($service as $data) { ?>
        <div class="col-lg-4 col-md-6 mt-5 d-flex align-items-stretch">
          <div class="icon-box">
            <div class="icon"><i class="bx <?= $data->icon ?>"></i></div>
            <h4><a href=""><?= $data->pelayanan ?></a></h4>
            <p><?= $data->deskripsi ?></p>
          </div>
        </div>
      <?php } ?>
    </div>

  </div>
</section><!-- End Services Section -->