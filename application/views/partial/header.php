 <!-- Header -->
 <header id="header" class="fixed-top">
     <div class="container d-flex align-items-center">

         <h2 class="logo mr-auto"><a href="">SANGKAR ENGINEERING</a></h2>
         <nav class="nav-menu d-none d-lg-block">
             <ul>
                 <li class="active"><a href="#hero">Home</a></li>
                 <li><a href="#about">About</a></li>
                 <li><a href="#services">Services</a></li>
                 <li><a href="#portfolio">Portofolio</a></li>
                 <li><a href="#team">Team</a></li>
                 <li><a href="#contact">Contact</a></li>
             </ul>
         </nav>
         <a href="<?= site_url('Auth') ?>" class="get-started-btn scrollto">Login</a>
     </div>
 </header>
 <!-- End Header