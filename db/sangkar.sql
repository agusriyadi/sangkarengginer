-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 13 Jun 2021 pada 03.36
-- Versi server: 10.5.5-MariaDB-log
-- Versi PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sangkar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `tittle` varchar(500) NOT NULL,
  `isi` varchar(5000) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `about`
--

INSERT INTO `about` (`id`, `tittle`, `isi`, `gambar`) VALUES
(1, 'SANGKAR ENGINEREEING', 'SOLVE YOUR DAILY TO YOUR MOST ADVANCED ENGINEERING PROBLEMS !!!Our Services :<ol><li>CAD (Autocad, Solidwork, Inventor)</><li>Simulation Static & Dynamic (Solidwork, Ansys, Altair)</li><li>Renewable Energy Consulting</li><li>Enviro Care</li></ol>Sangkar EngineeringRespect, Integrity, Growth, Honesty, Team Works', 'logo.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `contact`
--

INSERT INTO `contact` (`id`, `lokasi`, `email`, `phone`) VALUES
(1, 'Jl. Rancamulya, Rancamulya, Gabuswetan, Kabupaten Indramayu, Jawa Barat 45263', 'ti@gmail.com', '+62 82321750455');

-- --------------------------------------------------------

--
-- Struktur dari tabel `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `head1` varchar(1000) NOT NULL,
  `head2` varchar(1000) NOT NULL,
  `banner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `home`
--

INSERT INTO `home` (`id`, `head1`, `head2`, `banner`) VALUES
(1, 'SANGKAR ENGINEREEING', 'Respect, Integrity, Growth, Honesty, Team Works', 'undraw_Development_re_g5hq.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `portfolio`
--

CREATE TABLE `portfolio` (
  `id_portfolio` int(11) NOT NULL,
  `nama_port` varchar(50) NOT NULL,
  `kategori` enum('Web','App','Card','None') NOT NULL,
  `detail` varchar(1000) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `portfolio`
--

INSERT INTO `portfolio` (`id_portfolio`, `nama_port`, `kategori`, `detail`, `gambar`) VALUES
(1, 'E-cargo bike', 'None', 'Aplikasi untuk lomba secara online berbasis website, tingkat SD, SMP dan SMA sederajat se-kabupaten Indramayu', 'ecargobike (product display)2.png'),
(2, 'Turbo Charge', 'None', 'Aplikasi layanan cuci mobil panggilan untuk wilayah cirebon dan sekitarnya berbasis android', 'Cylinderheadp3 .jpg'),
(3, 'Cylinder', 'None', 'Aplikasi layanan cuci mobil panggilan untuk wilayah cirebon dan sekitarnya berbasis android', 'TurboChargerOverallAssembly.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `id_service` int(11) NOT NULL,
  `pelayanan` varchar(50) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `icon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`id_service`, `pelayanan`, `deskripsi`, `icon`) VALUES
(1, 'Design', 'Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi', 'bxl-dribbble'),
(2, 'Darfting', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore', 'bx-file'),
(3, 'Animation', 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia', 'bx-tachometer'),
(4, 'Statics', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis', 'bx-world'),
(5, 'Fluent Simulation', 'Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur', 'bx-slideshow'),
(6, 'Renewable Installation', 'Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur', 'bx-arch');

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `id_team` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`id_team`, `nama`, `jabatan`, `deskripsi`, `gambar`) VALUES
(1, 'Damar', 'DIVISI DESAIN,  DRAFTING AND ANIMATION', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'profildummy.png'),
(2, 'Endhy P', 'DIVISI DESAIN,  DRAFTING AND ANIMATION (MUSA) ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'profildummy.png'),
(3, 'anggi', 'DIVISI SIMULATION (DIMJI) ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'profildummy.png'),
(4, 'uknown', 'DIVISI RENEWABLE (KOHAN) ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'profildummy.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `role` enum('admin','operator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `role`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id_portfolio`);

--
-- Indeks untuk tabel `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`);

--
-- Indeks untuk tabel `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id_team`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id_portfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `service`
--
ALTER TABLE `service`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `team`
--
ALTER TABLE `team`
  MODIFY `id_team` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
